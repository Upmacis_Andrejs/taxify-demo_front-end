// Vendor resources (normalize.css, jquery and plugins by your choice)
require("./vendor/bootstrap.min.css");

// Your own CSS files
require("./scss/style.scss");

// Your own javascript files